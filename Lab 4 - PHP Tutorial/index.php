<?php
$host = 'localhost';
$username = 'root';
$password = '';
$dbname = 'cse455';

$dsn = 'mysql:host='. $host . ';dbname='. $dbname;

$currencyname = $_GET['currency'];
$amount = $_GET['currencyAmount'];

try {
  $pdo = new PDO($dsn, $username, $password);
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  
  $sql = 'SELECT * FROM currency WHERE currency_name=?';
  $stmt = $pdo->prepare($sql);
  $stmt->execute([$currencyname]);
  $posts = $stmt->fetch(PDO::FETCH_OBJ);
  
  $conObj->name = $posts->currency_name;
  $conObj->conversion = $posts->currency_rate * $amount;
  echo json_encode($conObj);
}
catch(PDOException $e){
  echo "Connection failed: " . $e->getMessage();
}
?>