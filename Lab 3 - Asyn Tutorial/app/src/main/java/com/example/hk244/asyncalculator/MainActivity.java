package com.example.hk244.asyncalculator;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity {
    //Declare variables
    private EditText editText01;
    private Button bnt01;
    private TextView textView01;
    private String usd;

    //The URL string will store the website for where the program will
    //get it's information for conversion.
    String url = "http://api.fixer.io/latest?base=USD";

    //It will store the USD value obtained formt he website
    String json = "";

    //Will assign later
    String line = "";

    //Will assign later
    String rate = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //cast the variables to their ids
        editText01 = findViewById(R.id.EditText01);
        bnt01 = findViewById(R.id.bnt);
        textView01 = findViewById(R.id.Yen);

        //Click event
        bnt01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View convertToYen){
                BackgroundTask task = new BackgroundTask();
                task.execute();
            }
        });
    }

    private class BackgroundTask extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute(){
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Void... values){
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(String result){
            super.onPostExecute(result);
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                URL web_url = new URL(MainActivity.this.url);

                HttpURLConnection httpURLConnection = (HttpURLConnection)web_url.openConnection();

                httpURLConnection.setRequestMethod("GET");

                httpURLConnection.connect();

                System.out.println("\nTesting ... Before connection method to URL \n");

                InputStream inputStream = httpURLConnection.getInputStream();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                System.out.println("Connection Successful\n");

                while(line != null){
                    line = bufferedReader.readLine();
                    json += line;
                }

                System.out.println("\n The JSON: " + json);
                JSONObject obj = new JSONObject(json);
                JSONObject objRate = obj.getJSONObject("rates");

                rate = objRate.get("JPY").toString();
                System.out.println("What is Rate? " + rate);
                Double value = Double.parseDouble(rate);
                //convert user's input to string
                usd = editText01.getText().toString();

                //if-else statement to make sure user can't leave the EditText blank
                if (usd.equals("")) {
                    textView01.setText("This field cannot be blank!");
                }
                else {
                    //Convert string to double
                    Double dInputs = Double.parseDouble(usd);

                    //Convert function
                    Double result = dInputs * value;

                    //Display the result
                    textView01.setText("$" + usd + " = " + "¥" + String.format("%.2f", result));

                    //Clear the editText after clicking
                    editText01.setText("");
                }
            }
            catch (MalformedURLException e) {
                e.printStackTrace();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
            catch (JSONException e) {
                Log.e("MYAPP"," unexpected JSON exception");
            }

            return null;
        }
    }
}
